#include <stdio.h>
/*Programa de ejemplo for anidado
que imprime la tabla del 2 a el 5.
*/
int main(int argc, char const *argv[]) {
  int ini=2;
  int fin=5;
  int j=0;
  printf("Dame el inicio: " );
    scanf("%d",&ini);
  printf("Dame el fin: " );
    scanf("%d",&fin);
if (ini>=fin) {
  printf("ini tiene que ser menor que fin\n" );
}else{

    for(ini;ini<=fin;ini++){
      for(j=1;j<=10;j=j+1){
        printf("%d x %d = %d \n",ini,j,ini*j);
      }
    printf("-----------------\n" );
  }
}
  return 0;
}
