#define MAX 100

struct libro{
  int id;
  char titulo[100];
  char autor[100];
};
typedef struct libro LIBRO;

LIBRO listaDeLibros[MAX];
int indiceAlArreglo=0;

void insertaLibro(LIBRO l){
  if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) {
    listaDeLibros[indiceAlArreglo]=l;
    indiceAlArreglo++;
  }else{
    printf("indiceAlArreglo fuera de rango[0 - %d]\n", MAX );
  }

}

void imprimeLibro(LIBRO l){
    printf("----------------\n");
    printf("%d\n",l.id );
    printf("%s\n",l.titulo );
    printf("%s\n",l.autor );
    printf("----------------\n");
}

void nuevoLibro(){
  LIBRO tmp;
  char temp;
  printf("Introduce el id:");
  scanf("%d", &tmp.id);
  scanf("%c",&temp);// limpiar buffer
  printf("Introduce el titulo:");
  fgets(tmp.titulo,100,stdin);
  printf("Introduce el autor:");
  fgets(tmp.autor,100,stdin);
  insertaLibro(tmp);
}

int menu(){
    int opcion=0;
    printf("\n----------- Menu para la aplicacion de BD para alumnos ---------\n");
    printf("(1) Crear lista.\n");
    printf("(2) Gurdar a un archivo.\n");
   printf("(3) Leer desde un archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar libro. \n");
    printf("(6) Uso futuro\n");
    printf("(7) Uso futuro\n");
    printf("(0) SALIR\n");
    printf("\n\nElige una opcion:");
    scanf("%d",&opcion);

    return opcion;
}
/*
 Funcion para grabar un ARREGLO DE REGISTROS
 en el archivo Libreria.dat
 */
void grabarRegistros(LIBRO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Libreria.dat","w"))==NULL){
        printf("El archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(LIBRO),tam,ptrF);
    }

    fclose(ptrF);
}

/*
 Funcion para LEER  REGISTROs
 en el archivo Evaluaciones.dat
 */
void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Libreria.dat","rb"))==NULL){
        printf("El archivo no se puede abrir\n");
    }
    else{
        //for /*(int i=0;i<tam;i++)*/
        fread(listaDeLibros,sizeof(LIBRO),tam,ptrF);
    }

    fclose(ptrF);
}

/**
 *
 * Regresa el numero de registros almacenados en el archivo
 *
 */
int registrosEnArchivo(){
    FILE *ptrF;
    int cont=0;
    LIBRO basura;
    if((ptrF=fopen("Libreria.dat","rb"))==NULL){
       // printf("El archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(LIBRO),1,ptrF))
                cont++;
        }

    }
    fclose(ptrF);
    return cont;
}

