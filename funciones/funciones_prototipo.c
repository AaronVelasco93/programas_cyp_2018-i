#include <stdio.h>
int  valormaximo (int, int);
int  valorminimo (int, int);

int main(int argc, char const *argv[]) {
  int x=0;
  int y=0;
  printf("Dame un valor: " );
    scanf("%d",&x);
  printf("Dame otro valor: " );
    scanf("%d",&y);
  printf("Resultado: %d",valormaximo(x,y));
  return 0;
}
/*
Funcion que regresa el mayor de dos numeros, en caso de que los numeros sean iguales regresa un -1
*/
int valormaximo(int a, int b){
  int max=0;
  if (a>b) {
    max=a;
  }else if (b>a){
    max=b;
  }else{
    max=-1;
  }
  return max;
}

//funcion No->2
int valorminimo(int a, int b){
  int min=0;
  if (a<b) {
    min=a;
  }else if (b<a){
    min=b;
  }else{
    min=-1;
  }
  return min;
}
