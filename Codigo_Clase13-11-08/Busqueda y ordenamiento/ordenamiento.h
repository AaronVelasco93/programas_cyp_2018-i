#define TAM 6

void imprime(int lista[])
{
  int j = 0;
  for(j = 0; j < TAM; j++)
  {
    printf("Lista [%d] = %d\n", j + 1, lista[j]);
  }
}

void busquedaLineal(int arreglo[], int buscado)
{
  int j = 0;
  for(j = 0; j < TAM; j++)
  {
    if(arreglo[j] == buscado)
    {
      printf("El dato esta en la posicion %d!\n", j + 1);
    }
  }
}

void ordenamientoBurbuja(int lista[])
{
  int i = 0, j = 0, temp = 0;

  for(i = 1; i < TAM; i++)
  {
    for(j = 0; j < TAM - 1; j++)
    {
      if(lista[j] > lista[j + 1])
      {
        temp = lista[j];
        lista[j] = lista[j + 1];
        lista[j + 1] = temp;
      }
    }
  }
}
