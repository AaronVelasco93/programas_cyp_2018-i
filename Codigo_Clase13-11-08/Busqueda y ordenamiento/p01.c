#include <stdio.h>
#include <stdlib.h>
#include "info.h"
#include "ordenamiento.h"

int main(int argc, char const *argv[]) {
  appInfoData("Ordenacion y busqueda","10/10/2016");
  int datos[6] = {32,43,344,65,86,4};

  imprime(datos);
  busquedaLineal(datos, 4);
  ordenamientoBurbuja(datos);
  imprime(datos);

  return 0;
}
