#include <stdio.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {
	int datoLeido;
	FILE *ptrArchivo;
	ptrArchivo=fopen("datos.dat","rb");
	
	if (ptrArchivo==NULL){
		printf("Error al abrir el archivo");
	}else {
		
		while(!feof(ptrArchivo)){
			
			fscanf(ptrArchivo,"%d",&datoLeido);
			printf("Dato: %d\n",datoLeido);
		}
		fclose(ptrArchivo);
	}
	
	return 0;
}
