#include <stdio.h>
#include "aritmetica.h"
int main(int argc, char const *argv[]) {
  int seleccion=1;
  while (seleccion ) {
    seleccion = menu();
    switch (seleccion) {
      case 1:
        suma();
        break;
      case 2:
        resta();
        break;
      case 0:
        printf("Saliendo\n");
        break;
      default:
        printf("Opcion invalida\n");
    }
  }
  return 0;
}
